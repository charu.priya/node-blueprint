const express = require('express');
const logger = require('../trace/logger');
const { getUser } = require('../dao/user-dao')

const router = express.Router();

/* GET users listing. */
router.get('/', async (req, res) => {
  try {
    logger.info('Received request for getting user list')
    const emailId = req.query.emailId;
    const userDetails = await getUser(emailId);
    res.status(200).json(userDetails);
  } catch (err) {
    logger.error(err)
    res.status(500).json({
      message: 'Failed while retrieving user details'
    });
  }
});

module.exports = router;
