/*
 * This file exposes api function consumed by individual services whenver api calling need arises
 * Api function expect apiOptions
*/
const request = require('request-promise')
const logger = require('../trace/logger')

async function api (apiOptions) {
  try {
    logger.info('Calling Api - Config')
    logger.info(JSON.stringify(apiOptions))
    return await request(apiOptions)
  } catch (err) {
    logger.error('Error in API request')
    logger.error(err);
    throw err
  }
}

module.exports = api
